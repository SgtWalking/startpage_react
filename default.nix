with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "react";
  buildInputs = [ nodejs ];

  shellHook = ''
    export PATH="$PWD/node_modules/.bin/:$PATH"
    npm i react react-router-dom json-server serve
  '';
}
