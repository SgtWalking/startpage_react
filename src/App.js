import { useState, useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Page from './components/Page';
import Menu from './components/Menu';
import Home from './components/Home';
import Wiki from './components/Wiki';
import WikiEntry from './components/WikiEntry';
import Helper from './components/Helper';

const URL = 'http://localhost:5000';

const App = () => {
  const [entries, setEntries] = useState([]);

  // Fetch wiki entry data from `json-server`
  useEffect(() => {
    const getEntries = async () => {
      const entries = await fetchEntries();
      setEntries(entries);
    };
    getEntries();
  }, []);

  const fetchEntries = async () => {
    const res = await fetch(`${URL}/entries`);
    const data = await res.json();
    return data;
  };

  return (
    <BrowserRouter>
      <div className="wrapper">
        <Menu />
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>

          <Route path="/Wiki" exact>
            <Wiki entries={entries} />
          </Route>

          {Object.keys(entries).map(entry => (
            <Route key={Helper.generateKey()} path={`/Wiki/${entry}`} exact>
              <Page
                headline={entry}
                component={<WikiEntry entryArr={entries[entry]} />}
              />
            </Route>
          ))}
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default App;
