class HELPER {
  /**
   * Generate a random number to be used as a key attribute for lists
   * @returns {number}
   */
  generateKey = () => Math.floor(Math.random() * 1000);

  /**
   * Check, if a given object is empty
   * @param {Object} obj Object to check
   * @returns {boolean}
   */
  isObjectEmpty = obj => {
    return !(
      obj &&
      Object.keys(obj).length === 0 &&
      obj.constructor === Object
    );
  };

  /**
   * Check if a given entry has a `table` or `code` property
   * @param {Object} obj Entry object
   * @param {'code' | 'table' | 'runs'} name Name of the prop to check for
   * @returns {boolean}
   */
  entryHasProp = (obj, name) => {
    return (
      obj.hasOwnProperty(name) &&
      (name === 'table'
        ? this.isObjectEmpty(obj[name])
        : obj[name] !== '')
    );
  };
}

const Helper = new HELPER();
export default Helper;
