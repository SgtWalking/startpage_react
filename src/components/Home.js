import Page from './Page';
import Sections from './Sections';

const Home = ({ headline }) => {
  const sections = [
    {
      name: 'chan',
      links: [
        { href: 'https://boards.4chan.org/g', name: '/g/' },
        { href: 'https://boards.4chan.org/n', name: '/n/' },
        { href: 'https://boards.4chan.org/out', name: '/out/' },
        { href: 'https://boards.4chan.org/wg', name: '/wg/' },
        { href: 'https://boards.4chan.org/wsg', name: '/wsg/' }
      ]
    },
    {
      name: 'entertainment',
      links: [{ href: 'https://youtube.com/', name: 'youtube' }]
    },
    {
      name: 'user',
      links: [
        { href: 'https://gmail.com/', name: 'gmail' },
        { href: 'https://mega.nz/', name: 'mega' }
      ]
    }
  ];

  return (
    <Page headline={headline} component={<Sections sections={sections} />} />
  );
};

Home.defaultProps = {
  headline: 'Home'
};

export default Home;
