import { Link } from 'react-router-dom';

const Menu = () => {
  return (
    <div className="menu">
      <Link to="/">Home</Link>
      <Link to="/Wiki">Wiki</Link>
      <a
        className="link"
        rel="noreferrer noopener"
        target="_blank"
        href="https://gitlab.com/SgtWalking"
      >
        gitlab
      </a>
    </div>
  );
};

export default Menu;
