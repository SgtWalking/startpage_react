const Page = ({ headline, sections, component }) => {
  return (
    <div className="page">
      <h1 className="headline">{headline}</h1>
      {sections ? (
        <div className="sections">{component}</div>
      ) : (
        <>{component}</>
      )}
    </div>
  );
};

Page.defaultProps = {
  headline: 'Page'
};

export default Page;
