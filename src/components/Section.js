import Helper from './Helper';

const Section = ({ section }) => {
  return (
    <div className="section">
      <h2 className="subhead">{section.name}</h2>
      <ul>
        {section.links.map(link => (
          <li key={Helper.generateKey()}>
            <a
              className="link"
              rel="noreferrer noopener"
              target="_blank"
              href={link.href}
            >
              {link.name}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Section;
