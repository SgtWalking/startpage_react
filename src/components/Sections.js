import Section from './Section';
import Helper from './Helper';

const Sections = ({ sections }) => {
  return (
    <div className="sections">
      {sections.map(section => (
        <Section key={Helper.generateKey()} section={section} />
      ))}
    </div>
  );
};

export default Sections;
