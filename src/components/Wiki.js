import Page from './Page';
import WikiEntries from './WikiEntries';

const Wiki = ({ entries, headline }) => {
  return (
    <Page headline={headline} component={<WikiEntries entries={entries} />} />
  );
};

Wiki.defaultProps = {
  headline: 'Wiki'
};

export default Wiki;
