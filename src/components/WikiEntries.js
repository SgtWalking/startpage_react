import { Link } from 'react-router-dom';
import Helper from './Helper';

const WikiEntries = ({ entries }) => {
  return (
    <>
      <p>List of articles</p>
      <ul>
        {Object.entries(entries).map(entry => (
          <li key={Helper.generateKey()}>
            <Link className="link" to={`/Wiki/${entry[0]}`}>
              {entry[0]}
            </Link>
            <br />
          </li>
        ))}
      </ul>
    </>
  );
};

export default WikiEntries;
