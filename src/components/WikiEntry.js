import Helper from './Helper';

const WikiEntry = ({ entryArr }) => {
  return entryArr.map(entry => (
    <div key={Helper.generateKey()} className="wiki__entry">
      <h2 className="entry__name">{entry.name}</h2>
      {Helper.entryHasProp(entry, 'table') && (
        <table>
          <thead>
            <tr>
              <th>{entry.table.header.config}</th>
              {Helper.entryHasProp(entry.table.header, 'runs') && (
                <th>{entry.table.header.runs}</th>
              )}
              <th>{entry.table.header.description}</th>
            </tr>
          </thead>
          <tbody>
            {entry.table.data.map(row => (
              <tr key={Helper.generateKey()}>
                <td>{row.config}</td>
                {Helper.entryHasProp(row, 'runs') && <td>{row.runs}</td>}
                <td>{row.description}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
      {Helper.entryHasProp(entry, 'code') && (
        <pre>
          <code>{entry.code.replace(/\t/g, '    ')}</code>
        </pre>
      )}
    </div>
  ));
};

export default WikiEntry;
